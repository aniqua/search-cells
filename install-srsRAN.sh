#!/bin/bash

#
# Install srsRAN
#

# exit when any command fails
set -e

# keep track of the last executed command
trap 'last_command=$current_command; current_command=$BASH_COMMAND' DEBUG

# echo an error message before exiting
trap 'echo "\"${last_command}\" command filed with exit code $?."' EXIT

PARENT="/local/tools"
SRSRAN="$PARENT"/srsran


#sudo apt-get update

sudo apt-get -y install --no-install-recommends build-essential cmake libfftw3-dev libmbedtls-dev libboost-program-options-dev libconfig++-dev libsctp-dev

mkdir -p $PARENT

cd $PARENT

if [ -d $SRSRAN ] 
then
    sudo rm -rf $SRSRAN
fi

hostname=$(cat /proc/sys/kernel/hostname)

echo "$hostname"

if [[ "$hostname" == *"nuc2"* ]]; then
  echo "Hostname = nuc2"
  git clone --recursive https://gitlab.flux.utah.edu/aniqua/srs-ran-nuc-2.git
  sudo mv srs-ran-nuc-2 srsran
else
	echo "Hostname = nuc1"
	git clone --recursive https://gitlab.flux.utah.edu/aniqua/srsran.git
	
fi

cd srsran

mkdir build

cd build

cmake ../

make

make test

sudo make install

srsran_install_configs.sh user

sudo ldconfig

exit 0