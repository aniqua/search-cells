#!/usr/bin/perl -w

use strict;
use Carp;
use English;
use Data::Dumper;
use Getopt::Std;
use File::Temp qw(tempfile);
use File::Basename;
use POSIX qw(isatty setsid);
use strict;
use List::Util qw(shuffle);
use POSIX qw[tzset];


# Drag in path stuff so we can find emulab stuff.
BEGIN { require "/etc/emulab/paths.pm"; import emulabpaths; }

#
# Install the monitor, run a few iterations, copy the CSV files to the
# the wbstore directory for transport back to the Mothership.
#
sub usage()
{
    print STDOUT "Usage: monitor [-dniV] [-t type] [-r radio] [-m duration] [-f channels]\n";
    exit(-1);
}

my $type;
my $radioID;
my $gain;
my $channelstr;
my $mode;
my $optlist     = "dniWr:t:c:m:f:o:l:";
my $noaction    = 0;
my $debug       = 0;
my $noinstall   = 0;
my $websave     = 0;
my $duration    = 1200;
my $CONFIG      = "/etc/monitor/device_config.json";
my $LOGFILE     = "/tmp/monitor.$$";
my $REPO        = dirname($PROGRAM_NAME);
my $INSTALL     = "$REPO/install.sh";
my $INSTALLNGINX= "$REPO/install-nginx.sh";
my $CELLSEARCH  = "/local/tools/srsran/build/lib/examples/cell_search";
my $UE          = "sudo srsue";
my $MONITORETC  = "/etc/monitor";
my $TAR         = "/bin/tar";
my $FIND        = "/usr/bin/uhd_find_devices";
my $PROBE       = "/usr/bin/uhd_usrp_probe";
my $FIXIT       = "/usr/lib/uhd/utils/b2xx_fx3_utils -D";
my $DOWNLOADER  = "/usr/bin/uhd_images_downloader";
my $CACHE       = "https://www.emulab.net/downloads/ettus/binaries/cache";
my $LOADER      = "/usr/bin/uhd_image_loader";
my $GENIGET     = "/usr/bin/geni-get";
my $GZIP        = "/bin/gzip";
my $REBOOT      = "/usr/local/bin/node_reboot";
my $IFACE       = "rf0";  # Someday we will be able to monitor others TXs
my $SAVEDIR     = "$VARDIR/save";
my $TMPDIR      = "/local/";
my $WEBDIR      = "/local/www";
my $LOOPS       = 1;
my $LOOPDELAYMIN= 0;
my $LOOPDELAYMAX= 2;
my $HOME        = $ENV{"HOME"};

#
# HOME will not be defined until new images are built.
#
if (!defined($HOME)) {
    $HOME = "/users/geniuser";
    $ENV{"HOME"} = $HOME;
    $ENV{"USER"} = "geniuser";
}

#
# Turn off line buffering on output
#
$| = 1;

# Protos
sub ProbeB210();
sub ProbeX310();
sub DownLoadImages($);
sub fatal($);
sub Notify($);
sub GetCellInfo($);
sub CheckDCI($);
sub GetSubframeCount($);

# For SENDMAIL
use libtestbed;

#
# Parse command arguments.
#
my %options = ();
if (! getopts($optlist, \%options)) {
    usage();
}
if (defined($options{"d"})) {
    $debug = 1;
}
if (defined($options{"i"})) {
    $noinstall = 1;
}
if (defined($options{"n"})) {
    $noaction = 1;
}
if (defined($options{"W"})) {
    $websave = 1;
}
if (defined($options{"t"})) {
    $type = $options{"t"};
}
if (defined($options{"c"})) {
    $LOOPS = $options{"c"};
}
if (defined($options{"r"})) {
    $radioID = $options{"r"};
}
if (defined($options{"f"})) {
    $channelstr = $options{"f"};
}
if (defined($options{"l"})) {
    $duration = int($options{"l"});
}
if (defined($options{"m"})) {
    $mode = int($options{"m"});
}

#
# Save off our output when not interactive, so that we can send it
# someplace useful. 
#
#if (! -t || ($viewer && !$debug)) {
if (!$debug) {
    open(STDOUT, ">> $LOGFILE") or
	die("opening $LOGFILE for STDOUT: $!");
    open(STDERR, ">> $LOGFILE") or
	die("opening $LOGFILE for STDERR: $!");
}

# We need the node ID for the output files.
my $nodeID = `cat $BOOTDIR/nodeid`;
if ($?) {
    fatal("Could not get nodeID");
}
chomp($nodeID);
my $domain = `cat $BOOTDIR/mydomain`;
if ($?) {
    fatal("Could not get domain");
}
chomp($domain);

#print "$domain\n";
#print "$nodeID\n";


# We do not run wbstore on the Mothership, so these have to copied to /proj.
# Need pid/eid below.
my $nickname = `cat $BOOTDIR/nickname`;
chomp($nickname);
my ($machine,$eid,$pid) = split(/\./, $nickname);

#
# We need the local XMLRPC cert/key in case we need to power cycle
# to bring the B210 back online.
#
if (! -e "$HOME/.ssl/emulab.pem") {
    if (! -e "$HOME/.ssl") {
	if (!mkdir("$HOME/.ssl", 0750)) {
	    fatal("Could not mkdir $HOME/.ssl: $!");
	}
    }
    system("$GENIGET rpccert > $HOME/.ssl/emulab.pem");
    if ($?) {
	fatal("Could not geni-get xmlrpc cert/key");
    }
}

# Install the monitor packages.
if (!$noinstall) {
    if (! -e "$MONITORETC/.ready") {
	system($INSTALL);
	if ($?) {
	    fatal("Could not install the monitor");
	}
	if (! -e "$MONITORETC/.ready") {
	    fatal("Monitor did not install properly");
	}
    }
    if ($websave && ! -e "/local/nginx-done") {
	system($INSTALLNGINX);
	if ($?) {
	    fatal("Could not install nginx");
	}
	if (! -e "/local/nginx-done") {
	    fatal("nginx did not install properly");
	}
    }
}

#
# Infer the type from node id. Fragile.
#

if (!defined($type)) {
    if ($nodeID =~ /nuc/ || $nodeID =~ /^ed\d+/) {
	$type = "B210";
    }
    else {
	$type = "X310";
    }
}
if ($type eq "B210") {
    ProbeB210();
}
elsif ($type eq "X310") {
    if (!defined($radioID)) {
	fatal("Must provide radio node ID with the -r option");
    }
    ProbeX310();
}
else {
    fatal("Do not know to probe radio type $type");
}

if ($noaction) {
    print "Exiting without doing anything\n";
    exit(0);
}

#
# Determine directory to save
#
if ($websave) {
$SAVEDIR = $WEBDIR;
}
elsif ($domain eq "emulab.net") {
#
# Ick, if we are running on the Mothership, have to write the file into
# /proj instead of wbstore. Lets create a tar file that looks like the
# wbstore file and has a known name.
#
$SAVEDIR = "/proj/$pid/exp/$eid";
}

#
# Determine name of the radio to use for saving data
#
my $radio_name = (split /\./, "$domain")[0];
my $busname;
if ($domain eq "emulab.net") {
    my $hostname = `cat /proc/sys/kernel/hostname`;
    $radio_name = (split /\./, "$hostname")[0];
    $radio_name = (split /\-/, "$radio_name")[1];
    chomp($radio_name);
}
else {
    if (index($radio_name, "bus") != -1) {
        $busname = $radio_name;
    }
    $radio_name = "${radio_name}-${machine}";
}

#
# Get the list of frequencies or bands
#
my @channels;
if (!defined($channelstr)) {
    fatal("No frequency or band specified.");
}
@channels = split(',', $channelstr);

my $tool;
if ($mode <= 2) {
    $tool = "cell_search";
}
elsif ($mode == 3) {
    $tool = "srsue";
}
else {
    fatal("Unrecognized mode");
}
$ENV{'TZ'} = 'America/Denver';
tzset();

#
# Run Cell search or srsUE. If things go smoothly, move it to the wbstore save directory.
#
 
while ($LOOPS) {

    my $now  = `date "+%m-%d-%Y_%H-%M-%S"`;
    chomp $now;
    my $name = "${tool}_${radio_name}_${now}";
    my $out = "${SAVEDIR}/${name}";
    chomp $out;
    system("mkdir $out");
    if ($?) {
        fatal("Could not create data save directory.");
    }


    if(defined($busname)) {
        system("gpspipe -w control.${busname}.powderwireless.net | tee ${out}/gps.txt &");
        if ($?) {
            fatal("Could not start gpspipe.");
        }
    }

    foreach my $c (@channels) 
    {
        my $cmd;
        if ($mode == 1) {
            $cmd = "${CELLSEARCH} -b ${c} -f ${out}/${c}.csv"
        }
        elsif ($mode == 2) {
            $cmd = "${CELLSEARCH} -c ${c} -f ${out}/${c}.csv"
        }
        else {
            $cmd = "sudo srsue --rat.eutra.dl_earfcn ${c} --log.all_level debug --log.filename ${out}/${c}.log /local/tools/srsran/srsue/ue.conf.example &"
        }
        
        system($cmd);
        if ($?) {
            #fatal("Could not run cmd.");
            print "Could not run cmd.\n";
            Notify("Could not run cmd.");
        }

        if ($mode == 3) {
            sleep($duration); 
            system("sudo pkill srsue");
        }
        sleep(30);
    }

    #
    # Compress and save
    #

    system("$TAR -zcvf $out.tar.gz  -C $SAVEDIR $name");
    if ($?) {
        print "Could not compress data.\n";
        print "$TAR -zcvf $out.tar.gz  -C $SAVEDIR $name\n";
        print "Reason: $!";  
    }

    if (!$websave && $domain eq "emulab.net") {
        my $mdir = "/proj/$pid/cell_search";
        if (! -e $mdir) {
            if (!mkdir($mdir, 0775)) {
            fatal("Could not mkdir $mdir: $!");
            }
        }
        system("/bin/mv $out.tar.gz $mdir/");
        if ($?) {
            fatal("Could not move dopey tar file into place");
        }
    }

    system("rm -rf $out");
    if(defined($busname)) {
        system("pkill gpspipe");
    }
       
    my $LOOPDELAY = int(rand($LOOPDELAYMAX-$LOOPDELAYMIN)) + $LOOPDELAYMIN;
    sleep($LOOPDELAY);    

    if ($type eq "B210") {
        ProbeB210();
    }
    elsif ($type eq "X310") {
        ProbeX310();
    }

    $LOOPS--;
    
}

if ($websave) {
    system("sudo /bin/touch $WEBDIR/DONE");
}
Notify("Worked") if ($debug);
exit(0);


#
# Probe a directly connected B210.
#
sub ProbeB210()
{
    if (DownLoadImages("b2xx")) {
	fatal("Could not download b2xx images");
    }
    #
    # Probe to see if we can find the B210. If not, power cycle.
    # We capture the output so we make sure its on USB 3 instead of 2.
    #
    my $output = `$PROBE 2>&1`;
    print $output;
    if ($?) {
	# Power cycle, but only once.
	if (-e "$MONITORETC/.rebooted") {
	    fatal("Could not find the radio after power cycle");
	}
	system("sudo /bin/touch $MONITORETC/.rebooted");
	system("/bin/sync");
	sleep(1);
	system("$REBOOT -s $nodeID");
	sleep(15);
	# Still here? Bad.
	fatal("Power cycle failed!");
    }
    if ($output =~ /Operating over USB (\d+)/) {
	if ($1 == 2) {
	    print "Attempting to fix USB\n";
	    system($FIXIT);
	    if ($?) {
		fatal("$FIXIT failed");
	    }
	    # Need a little delay before the probe else it fails.
	    sleep(5);
	    # Have to probe it again.
	    $output = `$PROBE 2>&1`;
	    if ($?) {
		fatal("Could not probe after USB fix");
	    }
	    if ($output !~ /Operating over USB 3/) {
		fatal("Not able to fix the USB level");
	    }
	}
    }
    else {
	fatal("Could not determine which USB is being used");
    }
    #
    # Create the config file.
    #
    system("sudo /bin/rm -f /tmp/device.cnf")
	if (-e "/tmp/device.cnf");
    
    open(CONFIG, "> /tmp/device.cnf") or
	fatal("Could not open config file for writing: $!");
    print CONFIG "{ \"devices\" : { \"${nodeID}:rf0\" : ".
	"{\"name\" : \"${nodeID}:rf0\", ".
	"\"channels\" : {\"0\" : \"RX2\"} } } }\n";
    close(CONFIG);
    system("sudo /bin/cp -f /tmp/device.cnf $CONFIG");
    if ($?) {
	fatal("Could not copy new file to $CONFIG");
    }
    # Default gain for B210s
    $gain = 85;
    return 0;
}

#
# Proble an X310 connected by ethernet link. IP is hardwired.
#
# Reflash: Do an uhd_images_downloader, then:
#   uhd_image_loader --args "type=x300,addr=192.168.40.2,fpga=XG"
# then power cycle.
#
sub ProbeX310()
{
    # Need this for X/N 310s
    system("sudo /sbin/sysctl -w net.core.wmem_max=24862979");

    if (DownLoadImages("x3xx")) {
	fatal("Could not download x3xx images");
    }

    #
    # Use find to see if its even available. 
    #
    my $output = `$FIND 2>&1`;
    print $output;
    if ($?) {
	print "Rebooting $radioID ...\n";
	system("$REBOOT -s $radioID");
	print "Waiting a bit for trying to find it again\n";
	sleep(30);
	
	$output = `$FIND 2>&1`;
	print $output;
	if ($?) {
	    fatal("Could not find X310 after power cycle");
	}
    }
    #
    # Probe it to see if it has the correct firmware.
    #
    $output = `$PROBE 2>&1`;
    print $output;
    if ($?) {
	if ($output =~ /Error: .* Expected FPGA/) {
	    print "Flashing the X310\n";
	    system("$LOADER --args='type=x300,addr=192.168.40.2,fpga=XG'");
	}
	if ($?) {
	    fatal("Could not load required FPGA firmware");
	}
	# Must reboot and wait.
	print "Rebooting $radioID ...\n";
	system("$REBOOT -s $radioID");
	print "Waiting a bit before trying to probe it again\n";
	sleep(30);
	# Probe again, bail if it fails again.
	$output = `$PROBE 2>&1`;
	print $output;
	if ($?) {
	    fatal("Probe failed after flashing");
        # To-do: try the flashing option
	}
    }
    #
    # Config file is different on an X310. And ... the cell radio is different
    # then the cbrs radio.
    #
    my $antenna = "RX2";
    
    system("sudo /bin/rm -f /tmp/device.cnf")
	if (-e "/tmp/device.cnf");
    
    open(CONFIG, "> /tmp/device.cnf") or
	fatal("Could not open config file for writing: $!");
    print CONFIG "{ \"is_bs\" : true, ".
	" \"devices\" : { \"${radioID}:rf0\" : ".
	"    {\"name\" : \"${radioID}:rf0\", ".
	"     \"channels\" : {\"0\" : \"${antenna}\"} } } }\n";
    close(CONFIG);
    system("sudo /bin/cp -f /tmp/device.cnf $CONFIG");
    if ($?) {
	fatal("Could not copy new file to $CONFIG");
    }
    # Default gain for X310s
    $gain = 10;
}

sub Notify($)
{
    my ($mesg) = $_[0];

    if (! -t) {
	SENDMAIL("aniqua\@cs.utah.edu",
		 "Spectrum Monitoring Experiment: ${nodeID}.${domain}",
		 $mesg . "\n\n", undef,
		 "X-Spectrum-Monitor: ${nodeID}.${domain} ",
		 $LOGFILE);
	# So it has time to depart before experiment termination.
	sleep(10);
    }
    
}

#
# Having some problems with image downloading, so try more then once.
#
sub DownLoadImages($)
{
    my ($type) = @_;

    print "Downloading image type $type\n";

    system("sudo $DOWNLOADER -b $CACHE -t $type");
    if ($?) {
	sleep(5);
	system("sudo $DOWNLOADER -b $CACHE -t $type");
	return -1
	    if ($?);
    }
    return 0;
}

my $exiting = 0;

sub fatal($)
{
    my ($mesg) = $_[0];
    $exiting = 1;
    Notify($mesg);
    if ($domain eq "emulab.net") {
    system("/bin/cp $LOGFILE /proj/$pid");
    }

    die("*** $0:\n"."    $mesg\n");
}

END {
    return
	if ($exiting);
    
    if ($?) {
	my $exitstatus = $?;
	Notify("Unexpected exit");
	$? = $exitstatus;
    }
}
